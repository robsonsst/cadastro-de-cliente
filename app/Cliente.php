<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cliente extends Model{
    //Para os dados da tabela não serem acessados de forma externa
    protected $fillable = ['nome', 'email', 'cpf', 'telefone', 'endereco', 'dt_nasc', 'genero'];
}