<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class RequestCliente extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(){
        /** para não ter que criar 2 arquivos para o request, foi add um if verificando se é um método do tipo PUT (edição) ou POST (add) */
        if($this->method() == "PUT") {
        
            return [
                'nome_cliente' => ['required', 'string', 'max:40','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/'],
                'cpf_cliente' => ['required', 'numeric'],
                'telefone_cliente' => ['required', 'numeric'],
                'endereco_cliente' => ['required','string','max:255'],
                'genero_cliente' => ['required', 'string', 'max:10','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/']
            ];                
        }
        if($this->method() == "POST") {
        
            return[
                'nome_cliente' => ['required', 'string', 'max:40','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/'],
                'cpf_cliente' => ['required', 'numeric'],
                'telefone_cliente' => ['required', 'numeric'],
                'endereco_cliente' => ['required','string','max:255'],
                'dt_nasc_cliente' => ['required', 'date'],
                'genero_cliente' => ['required', 'string', 'max:10','regex:/^([a-zA-Zà-úÀ-Ú]|-|_|\s)+$/']
            ];
        }
    }
    public function attributes(){
        return [
            'nome_cliente' => 'Nome',
            'email_cliente' => 'E-mail',
            'cpf_cliente' => 'CPF',
            'telefone' => 'Número de telefone',
            'endereco_cliente' => 'Endereço',
            'genero_cliente' => 'Gênero'
        ];
    }
    public function messages(){
        return [];
    }
}
