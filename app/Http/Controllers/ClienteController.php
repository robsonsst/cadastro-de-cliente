<?php

namespace App\Http\Controllers;

use App\Cliente;
use Illuminate\Http\Request;
use App\Http\Requests\RequestCliente;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\DB;
// use Illuminate\Support\Facades\Session;
use DataTables;
use Session;

class ClienteController extends Controller{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        return view('Cliente.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(){
        return view('Cliente.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RequestCliente $request){
        try{
            $cliente = new Cliente();
            $cliente->nome = $request->nome_cliente;
            $cliente->email = $request->email_cliente;
            $cliente->cpf = $request->cpf_cliente;
            $cliente->telefone = $request->telefone_cliente;
            $cliente->endereco = $request->endereco_cliente;
            $cliente->dt_nasc = $request->dt_nasc_cliente;
            $cliente->genero = $request->genero_cliente;

            //serve para assegurar que se der um erro em alguma fase durante o processo, não irá para o banco
            DB::transaction(function() use ($cliente){
                $cliente->save();
            });
            
            Session::flash('mensagem', 'Cliente cadastrado!');
            
            return Redirect::to('/cliente');
        }
        catch(\Exception $error){
            //Traz de volta para a os campos os valores que já foram digitados
            Session::flash('mensagem', 'Não foi possível cadastrar o cliente!');
            return back()->withInput();
         }
        //dd($request->nome);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $cliente = Cliente::get();
        return Datatables::of($cliente)
        ->editColumn('acao', function ($cliente) {    
            return '
                <div class="btn-group btn-group-sm">
                    <a href="/cliente/'.$cliente->id.'/edit"
                        class="btn btn-info"
                        title="Editar" data-toggle="tooltip">
                        <i class="fas fa-pencil-alt"></i>
                    </a>
                    <a href="#" 
                        class="btn btn-danger btnExcluir"
                        data-id="'.$cliente->id.'"
                        title="Excluir" data-toggle="tooltip">
                        <i class="fas fa-trash"></i>
                    </a>
                </div>';
        })
        ->escapeColumns([0])
        ->make(true);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id){
        $cliente = Cliente::find($id);

        return view('Cliente.edit', compact('cliente'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(RequestCliente $request, $id){
        try{
            $cliente = Cliente::find($id);
            $cliente->nome = $request->nome_cliente;
            $cliente->email = $request->email_cliente;
            $cliente->cpf = $request->cpf_cliente;
            $cliente->telefone = $request->telefone_cliente;
            $cliente->endereco = $request->endereco_cliente;
            $cliente->dt_nasc = $request->dt_nasc_cliente;
            $cliente->genero = $request->genero_cliente;

            //serve para assegurar que se der um erro em alguma fase durante o processo, não irá para o banco
            DB::transaction(function() use ($cliente){
                $cliente->save();
            });
            
            Session::flash('mensagem', 'Cliente atualizado!');
            
            return Redirect::to('/cliente');
        }
        catch(\Exception $error){
            //Traz de volta para a os campos os valores que já foram digitados
            Session::flash('mensagem', 'Não foi possível atualizar os dados do cliente!');
            return back()->withInput();
         }

         return Redirect::to('/cliente/'.$cliente->id.'/edit');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id){
        try {
            $cliente = Cliente::find($id);
            $cliente->delete();
            
            return response()->json(array('status' => "OK"));
        }catch (\Exception  $erro) {
            return response()->json(array('erro' => "ERRO"));
        }
    }
}
