@extends('layouts.app')
@section('htmlheader_titulo', 'Cadastrar Cliente')

@section('scripts_adicionais')
    <script type="text/javascript" src=" {{asset('plugins/maskedinput/jquery.maskedinput.min.js')}}"></script>
    <script  type="text/javascript" >
        $(document).ready( function($){
            $("#cpf_cliente").mask("999.999.999-99");
        });   
        $(document).ready( function($){
            $("#telefone_cliente").mask("(99) 9 9999-9999");
        }); 
    </script>
@endsection

@section('conteudo')
    <div class="card">
        <section class="content-header">
            <div class="col-sm-12">
                <h2>Cadastro de Clientes</h2>
            </div>
        </section>
        @if(Session::has('mensagem'))
            <div class="alert alert-danger alert-dismissible">
                <!-- data-dimiss - fecha o button que abrir sem precisar de nada-->
                <button type="button" class="close" data-dimiss="alert">x</button>
                <h5><i class="icon das fa-ban"></i>Atenção</h5>
                {{Session::get('mensagem')}}
            </div>
        @endif
        <div class="card-body">
            <div class="container">
                <form action="/cliente" method="POST">
                    @csrf
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>Nome Completo</label> <br>
                            <input type="text" name='nome_cliente' class="form-control @error ('nome_cliente') is-invalid @enderror" value="{{old('nome_cliente')}}">
                            @error('nome_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-3"> 
                            <label>Email</label> <br>
                            <input type="text" name='email_cliente' class="form-control @error ('email_cliente') is-invalid @enderror" value="{{old('email_cliente')}}">
                            @error('email_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-2"> 
                            <label>CPF</label> <br>
                            <input id = "cpf_cliente" type="text" name='cpf_cliente' class="form-control @error ('cpf_cliente') is-invalid @enderror" value="{{old('cpf_cliente')}}">
                            @error('cpf_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-2"> 
                            <label>Telefone</label> <br>
                            <input id="telefone_cliente" type="text" name='telefone_cliente' class="form-control @error ('telefone_cliente') is-invalid @enderror" value="{{old('telefone_cliente')}}">
                            @error('telefone_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-3"> 
                            <label>Endereço</label> <br>
                            <textarea type="text" name="endereco_cliente" placeholder="Rua A, Centro, Jequié..." row="10" class="form-control @error ('endereco_cliente') is-invalid @enderror">{{old('endereco_cliente')}}</textarea>
                            @error('endereco_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-2"> 
                            <label>Data de Nascimento</label> <br>
                            <input type="date" name='dt_nasc_cliente' class="form-control @error ('dt_nasc_cliente') is-invalid @enderror" value="{{old('dt_nasc_cliente')}}">
                            @error('dt_nasc_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror
                        </div>
                        <div class="form-group col-md-2"> 
                            <label>Genero</label> <br>
                            <input type="text" name='genero_cliente' class="form-control @error ('genero_cliente') is-invalid @enderror" value="{{old('genero_cliente')}}">
                            @error('genero_cliente') 
                                <span class="invalid-feedback" role="alert">
                                    <strong>({$message})</strong>
                                </span>
                            @enderror  
                        </div>

                        <div>  
                            <button type="submit" class="btn btn-info float-right" style="margin:32px 0 0 50px;">Enviar</button> 
                        </div>
                        <div> 
                            <a href="/cliente" class="btn btn-outline-info float-right" style="margin:32px 10px 30px"><b>Voltar</b></a>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection