$(document).ready( function($){    
    base_url = window.location.origin;

    var table = $("#clientes").DataTable({   
        ajax: base_url+"/cliente/show",
        serverSide: true,
        reponsive:true,
        processing:true,
        searching: true,
        "order":[0,"desc"],
        columns: [
            {"width":"5%",data:"id",name:"id"},
            {"width":"18%",data:"nome",name:"nome"},
            {"width":"10%",data:"email",name:"email"},
            {"width":"10%",data:"cpf",name:"cpf"},
            {"width":"50%",data:"telefone",name:"telefone"},
            {"width":"25%",data:"endereco",name:"endereco"},
            {"width":"5%",data:"dt_nasc",name:"dt_nasc"},
            {"width":"5%",data:"genero",name:"genero"},
            {"width":"10%",data:"acao",name:"acao"},
        ],
    });

    $(document).on("click", ".btnExcluir", function(){    
        id = $(this).data('id')
        $.ajax({
            type: "delete",
            url: base_url + "/cliente/" + id,
            dataType: 'json',
            crossDomain: true,
            contentType: "application/json",
            headers: {"X-CSRF-TOKEN": $('meta[name="csrf-token"]').attr("content")},
            success: function(){
                location.reload();
            },
            error: function(){
                alert("Não foi possível excluir")
            }
        });
    });
});